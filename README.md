Налаштування:
iOS 13+, iPhone Only, Vertical Interface Only
UIkit або SwiftUI, MVVM в пріоритеті

Задача:
Реалізувати додаток для перегляду фотографій з марсоходів використовуючи NASA API
https://api.nasa.gov/index.html#browseAPI - Mars Rover Photos (auth key можна отримати за формою https://api.nasa.gov (https://api.nasa.gov/) ).

- Необхідно дати можливість користувачу обирати тип марсохода, камери та дати. 
- Реалізувати посторінкове завантаження списку (lazy load). 
- По натисканню на інформацию видображати фулскріново зображення з можливістю рісайза та скрола.
- Використовувати кешування зображень

Дизайн - https://www.figma.com/file/hS1HYIVr0lqACogIaZcAIi/Test?type=design&amp;node-id=0-1&mode=design

Буде плюсом:
Результат пошуку/переглядів зберігати в БД (Realm або CoreData) з можливістю переглянути історію запитів на екрані.
У випадку UIKit компонети повинні бути реалізовані Storyboard + Programmatically (приблизно у співідношенні по 50%)
